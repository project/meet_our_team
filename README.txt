*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
                                MEET OUR TEAM
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

CONTENTS OF THIS FILE:
----------

- Introduction
- Who can use
- Features
- Requirements
- Installation
- Configuration
- Customization
- Un-installation
- Maintainer

*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

INTRODUCTION:
----------

To display the team members on a single page in grid format with special effects
for each member information.

Easily identify the hierarchical structure of organization. Who are all involved
in the organization with their names, designation and other social media links.

*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

WHO CAN USE:
----------

- Any corporate companies can use to showcase the details of different level in
the organization structures.
- For promotion aspects, showing N number of team members (staff).

*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

FEATURES:
--------

- Easy configuration for administrator for this page
- Jquery effects to display the information about each member
- Selection option for Image Frame / Background Shapes
- Define the position for content and image
- Maintain custom CSS classes for container
- Choose order to display the members in the page
- Members social contacts information like Facebook,
twitter, Google+, YouTube etc.
- No page creation required, a page is provided by the module to display the
members information,

*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

REQUIREMENTS:
----------

Two dependencies modules are required:

- Entity (https://www.drupal.org/project/entity)
- Views (https://www.drupal.org/project/views)

*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

INSTALLATION:
----------

To install this module:

- Copy meet_our_team folder to modules directory.
- At '/admin/modules' enable the 'Meet our team' module.

*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

CONFIGURATION:
----------

- Update the settings at Configuration » People » Meet our team
(/admin/config/people/meet-our-team/settings)
- For better result, change 'Number of columns' to 1, if “Both”
option is selected in 'Display content'

NOTE: Cache clearing is always recommended after any configuration changes done,
as dynamic fields are created based upon configuration selection

*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

CUSTOMIZATION:
----------

- Team members can be added by creating Meet our team content nodes
(Content >> Add content >> Meet our team) and the listing can be
seen at '/meet-our-team'
- To have a custom path update the View page path and URL for
Back to team" link in '/themes/node--meet_our_team.tpl.php'

*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

UN-INSTALLATION:
----------

To un-install the module,
1) Go to admin/modules
2) Find Meet our team package
3) Un-check the checkbox and save.
4) Then go to admin/modules/uninstall
5) Un-check the checkbox and click 'uninstall'.

*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

MAINTAINER:
----------

Madan Gopal (madan879) | https://www.drupal.org/u/madan879
Nishad Bhide (bhide.nishad) | https://www.drupal.org/u/bhidenishad

*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
