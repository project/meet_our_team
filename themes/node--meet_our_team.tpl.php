<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
 ?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="mot cls-full">
    <div class="cls-left cls-row-width-5">
      <div><?php print render($content['meet_our_team_image']); ?></div>
      <p><?php print check_plain($node->title); ?></p>
      <p><span><?php print render($content['meet_our_team_designation'][0]['#markup']); ?></span></p>
      <div class="social-icon">
        <?php if (variable_get('meet_our_team_social_email') && isset($node->meet_our_team_email) && !empty($node->meet_our_team_email)) { ?>
          <a class="email" href="mailto:<?php print render($content['meet_our_team_email'][0]['#markup']);?>" title="Email"></a>
        <?php } ?>
        <?php if (variable_get('meet_our_team_social_facebook') && isset($node->meet_our_team_facebook) && !empty($node->meet_our_team_facebook)) { ?>
          <a class="facebook" href="<?php print render($content['meet_our_team_facebook'][0]['#markup']);?>"  target="_blank" title="Facebook"></a>
        <?php } ?>
        <?php if (variable_get('meet_our_team_social_twitter') && isset($node->meet_our_team_twitter) && !empty($node->meet_our_team_twitter)) { ?>
          <a class="twitter" href="<?php print render($content['meet_our_team_twitter'][0]['#markup']);?>"  target="_blank" title="Twitter"></a>
        <?php } ?>
        <?php if (variable_get('meet_our_team_social_google') && isset($node->meet_our_team_google) && !empty($node->meet_our_team_google)) { ?>
          <a class="google" href="<?php print render($content['meet_our_team_google'][0]['#markup']);?>" target="_blank"  title="Google+"></a>
        <?php } ?>
        <?php if (variable_get('meet_our_team_social_linkedin') && isset($node->meet_our_team_linkedin) && !empty($node->meet_our_team_linkedin)) { ?>
          <a class="linkedin" href="<?php print render($content['meet_our_team_linkedin'][0]['#markup']);?>" target="_blank" title="LinkedIn"></a>
        <?php } ?>
        <?php if (variable_get('meet_our_team_social_youtube') && isset($node->meet_our_team_youtube) && !empty($node->meet_our_team_youtube)) { ?>
          <a class="youtube" href="<?php print render($content['meet_our_team_youtube'][0]['#markup']);?>"  target="_blank" title="Youtube"></a>
        <?php } ?>
        <?php if (variable_get('meet_our_team_social_pinterest') && isset($node->meet_our_team_pinterest) && !empty($node->meet_our_team_pinterest)) { ?>
          <a class="pinterest" href="<?php print render($content['meet_our_team_pinterest'][0]['#markup']);?>"  target="_blank" title="Pinterest"></a>
        <?php } ?>
        <?php if (variable_get('meet_our_team_social_instragram') && isset($node->meet_our_team_instragram) && !empty($node->meet_our_team_instragram)) { ?>
          <a class="instragram" href="<?php print render($content['meet_our_team_instragram'][0]['#markup']);?>"  target="_blank" title="Instragram"></a>
        <?php } ?>
      </div>
    </div>
    <div class="cls-75">
      <div class="clsbody"><?php print render($content['body']); ?></div>
    </div>
  </div>
  <div class="back-mot">
    <a href="<?php print url('meet-our-team');?>"><?php print t('<< Back to team'); ?></a>
  </div>
  <div class="line-separator"></div>
</div>
