<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field.
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<div class="<?php echo (!empty($info_class) ? $info_class : 'cls-left cls-row-width-' . $no_cols); ?>">
<!-- Showing Image and information -->
<?php
  if ($content_type == 'image_only') { ?>
    <div class="<?php echo $class_image;?> <?php echo ($frame != 'none') ? $frame : ''; ?> cls-width">
    <?php print $view->render_field("meet_our_team_image", $view->row_index); ?>
      <div class="<?php echo ($jeffect == 'on') ? $class_caption : 'mot-text-caption'; ?>">
      <!-- Showing title and designation -->
      <?php	foreach ($display_info as $info) {
        if (trim($info) == '%%meet_team_title%%') { ?>
          <div class="mot-title">
            <?php print $fields["title"]->content; ?>
         </div>
        <?php }
       if (trim($info) == '%%meet_team_designation%%') { ?>
         <div class="mot-destignation">
           <?php print $view->render_field("meet_our_team_designation", $view->row_index); ?>
         </div>
       <?php }
      } ?>
     </div>
    </div>
    <!-- Showing read more -->
    <?php if (!empty($readmore)) { ?>
      <div class="read-more">
        <a href="<?php print $href; ?>" <?php print $target; ?>><?php echo $readmore; ?></a>
      </div>
    <?php } ?>
    <!-- Showing text only -->
  <?php
  } elseif ($content_type == 'text_only') { ?>
    <div class="cls-text">
      <div class="mot-body">
        <?php print $view->render_field("body", $view->row_index); ?>
      </div>
      <!-- Showing read more -->
     <?php if (!empty($readmore)) { ?>
       <div class="read-more">
         <a href="<?php print $href; ?>" <?php print $target; ?>><?php echo $readmore; ?></a>
       </div>
     <?php } ?>
    </div>
    <div class="line-separator"></div>
<!-- Showing Image, information and text -->	
  <?php
  } else { ?>
    <div class="<?php echo $clsimg;?>">
      <div class="<?php echo $class_image;?> <?php echo ($frame != 'none') ? $frame : ''; ?> cls-width">
        <?php print $view->render_field("meet_our_team_image", $view->row_index); ?>
        <div class="<?php echo ($jeffect == 'on') ? $class_caption : 'mot-text-caption'; ?>">
        <!-- Showing title and designation --> 
        <?php foreach ($display_info as $info) {
          if (trim($info) == '%%meet_team_title%%') { ?>
            <div class="mot-title">
              <?php print $fields["title"]->content; ?>
            </div>
          <?php }
          if (trim($info) == '%%meet_team_designation%%') { ?>
            <div class="mot-destignation">
              <?php print $view->render_field("meet_our_team_designation", $view->row_index); ?>
            </div>
          <?php }
        } ?>
        </div>
      </div>
    </div>
    <div class="<?php echo $clstxt;?> cls-text">
      <div class="mot-body">
        <?php print $view->render_field("body", $view->row_index);  ?>
      </div>
      <!-- Showing read more -->
      <?php if (!empty($readmore)) { ?>
        <div class="read-more">
          <a href="<?php print $href; ?>" <?php print $target; ?>><?php print $readmore; ?></a>
        </div>
      <?php } ?>
    </div>
    <div class="line-separator"></div>
  <?php } ?>
</div>
