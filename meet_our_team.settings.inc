<?php

/**
 * @file
 * Maintain the configuration to display the team members.
 */

/**
 * Form constructor for the meet our team configuration form.
 *
 * @ingroup forms
 */
function meet_our_team_configuration_form($form, &$form_state) {
  // Defining empty form array.
  $form = array();
  $options['image_only'] = t('Image only');
  $options['text_only'] = t('Text only');
  $options['both'] = t('Both');

  $jquery_options['on'] = t('On');
  $jquery_options['off'] = t('Off');

  $jeffect_options['fade'] = t('Fade image');
  $jeffect_options['slide'] = t('Slide up (Name, Designation) information');

  $position_options['right'] = t('Right');
  $position_options['left'] = t('Left');

  $click_options['newtab'] = t('Open a new tab');
  $click_options['samepage'] = t('Open in same page');

  $frame_options['none'] = t('Square frame');
  $frame_options['circle'] = t('Circle frame');
  $frame_options['rounded'] = t('Rounded frame');

  $order_options['title'] = t('Title');
  $order_options['designation'] = t('Designation');
  $order_options['display_no'] = t('Display order number');

  $personal_options['yes'] = t('Yes');
  $personal_options['no'] = t('No');

  for ($i = 1; $i < 11; $i++) {
    $custom_time_interval = $i * 1000;
    $slide_interval_options[$custom_time_interval] = $custom_time_interval;
  }

  $form['meet_our_team_no_columns'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of columns'),
    '#default_value' => variable_get('meet_our_team_no_columns', 3),
    '#size' => 30,
    '#description' => t("Maximum use 10 to display team members in the single row."),
    '#required' => TRUE,
  );

  $form['meet_our_team_jquery_effect'] = array(
    '#type' => 'radios',
    '#options' => $jquery_options,
    '#title' => t('Jquery effect'),
    '#description' => t('On mouse over effect for image.'),
    '#default_value' => variable_get('meet_our_team_jquery_effect', t('off')),
    '#required' => TRUE,
  );

  $form['meet_our_team_jeffect_type'] = array(
    '#type' => 'radios',
    '#options' => $jeffect_options,
    '#title' => t('Effect type'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_jeffect_type', t('slide')),
    '#states' => array(
      'visible' => array(
        ':input[name="meet_our_team_jquery_effect"]' => array('value' => t('on')),
      ),
    ),
  );

  $form['meet_our_team_read_more'] = array(
    '#type' => 'textfield',
    '#title' => t('Default read more label'),
    '#default_value' => variable_get('meet_our_team_read_more', t('Read More')),
    '#size' => 30,
    '#description' => '',
  );

  $form['meet_our_team_display_content'] = array(
    '#type' => 'radios',
    '#options' => $options,
    '#title' => t('Display content'),
    '#description' => t('To display format of the content.'),
    '#default_value' => variable_get('meet_our_team_display_content', t('image_only')),
    '#required' => TRUE,
  );

  $form['meet_our_team_image_position'] = array(
    '#type' => 'radios',
    '#options' => $position_options,
    '#title' => t('Image position'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_image_position', t('right')),
    '#states' => array(
      'visible' => array(
        ':input[name="meet_our_team_display_content"]' => array('value' => t('both')),
      ),
    ),
  );

  $form['meet_our_team_text_position'] = array(
    '#type' => 'radios',
    '#options' => $position_options,
    '#title' => t('Text position'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_text_position', t('left')),
    '#states' => array(
      'visible' => array(
        ':input[name="meet_our_team_display_content"]' => array('value' => t('both')),
      ),
    ),
  );

  $form['meet_our_team_image_frame'] = array(
    '#type' => 'radios',
    '#options' => $frame_options,
    '#title' => t('Image background/frame'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_image_frame', t('none')),
    '#states' => array(
      'invisible' => array(
        ':input[name="meet_our_team_display_content"] ' => array('value' => t('text_only')),
      ),
    ),
  );

  $form['meet_our_team_display_info'] = array(
    '#type' => 'textfield',
    '#title' => t('Display information'),
    '#default_value' => variable_get('meet_our_team_display_info', t('%%meet_team_title%%, %%meet_team_designation%%')),
    '#size' => 60,
    '#description' => t("The information required to display in the page."),
    '#required' => TRUE,
  );

  $form['meet_our_team_click_action'] = array(
    '#type' => 'radios',
    '#options' => $click_options,
    '#title' => t('Click action'),
    '#description' => t('Action to be taken after clicking on Read More.'),
    '#default_value' => variable_get('meet_our_team_click_action', t('samepage')),
    '#required' => TRUE,
  );

  $form['meet_our_team_slide_interval'] = array(
    '#type' => 'select',
    '#options' => $slide_interval_options,
    '#title' => t('Slide open time (in miliseconds)'),
    '#description' => t('Select the time for slide to open'),
    '#default_value' => variable_get('meet_our_team_slide_interval', 2000),
    '#required' => TRUE,
  );

  $form['meet_our_team_container_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Default CSS class - outer container'),
    '#default_value' => variable_get('meet_our_team_container_class', ''),
    '#size' => 60,
    '#description' => t("If outer container is responsive, add relevant classes."),
  );

  $form['meet_our_team_info_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Default CSS class - info section'),
    '#default_value' => variable_get('meet_our_team_info_class', ''),
    '#size' => 60,
    '#description' => t("If info section is responsive, add relevant classes."),
  );

  $form['meet_our_team_order_by'] = array(
    '#type' => 'radios',
    '#options' => $order_options,
    '#title' => t('Order by'),
    '#description' => t('For displaying team member by order.'),
    '#default_value' => variable_get('meet_our_team_order_by', t('title')),
    '#required' => TRUE,
  );

  $form['meet_our_team_personal_url'] = array(
    '#type' => 'radios',
    '#options' => $personal_options,
    '#title' => t('Personal URLs'),
    '#description' => t('For allowing to displaying personal URL.'),
    '#default_value' => variable_get('meet_our_team_personal_url', t('no')),
    '#required' => TRUE,
  );

  $form['meet_our_team_social_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Email ID'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_social_email', FALSE),
    '#states' => array(
      'invisible' => array(
        ':input[name="meet_our_team_personal_url"]' => array('value' => t('no')),
      ),
    ),
  );

  $form['meet_our_team_social_facebook'] = array(
    '#type' => 'checkbox',
    '#title' => t('Facebook'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_social_facebook', FALSE),
    '#states' => array(
      'invisible' => array(
        ':input[name="meet_our_team_personal_url"]' => array('value' => t('no')),
      ),
    ),
  );

  $form['meet_our_team_social_twitter'] = array(
    '#type' => 'checkbox',
    '#title' => t('Twitter'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_social_twitter', FALSE),
    '#states' => array(
      'invisible' => array(
        ':input[name="meet_our_team_personal_url"]' => array('value' => t('no')),
      ),
    ),
  );

  $form['meet_our_team_social_google'] = array(
    '#type' => 'checkbox',
    '#title' => t('Google+'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_social_google', FALSE),
    '#states' => array(
      'invisible' => array(
        ':input[name="meet_our_team_personal_url"]' => array('value' => t('no')),
      ),
    ),
  );

  $form['meet_our_team_social_linkedin'] = array(
    '#type' => 'checkbox',
    '#title' => t('LinkedIn'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_social_linkedin', FALSE),
    '#states' => array(
      'invisible' => array(
        ':input[name="meet_our_team_personal_url"]' => array('value' => t('no')),
      ),
    ),
  );

  $form['meet_our_team_social_youtube'] = array(
    '#type' => 'checkbox',
    '#title' => t('Youtube'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_social_youtube', FALSE),
    '#states' => array(
      'invisible' => array(
        ':input[name="meet_our_team_personal_url"]' => array('value' => t('no')),
      ),
    ),
  );

  $form['meet_our_team_social_instragram'] = array(
    '#type' => 'checkbox',
    '#title' => t('Instragram'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_social_instragram', FALSE),
    '#states' => array(
      'invisible' => array(
        ':input[name="meet_our_team_personal_url"]' => array('value' => t('no')),
      ),
    ),
  );

  $form['meet_our_team_social_pinterest'] = array(
    '#type' => 'checkbox',
    '#title' => t('Pinterest'),
    '#description' => '',
    '#default_value' => variable_get('meet_our_team_social_pinterest', FALSE),
    '#states' => array(
      'invisible' => array(
        ':input[name="meet_our_team_personal_url"]' => array('value' => t('no')),
      ),
    ),
  );

  // Return system settings form.
  return system_settings_form($form);
}
