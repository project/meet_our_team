/**
 * @file
 * Implement a custom javascript.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.meet_our_team = {
    attach: function (context) {
      $('#team-member-info').delay(1000).hide('slow');
      $('.mot-image').hover(function () {
        $('.mot-caption', this).slideToggle('slow');
      }, function () {
        $('.mot-caption', this).slideToggle('slow');
      });

      $('.mot-image-fade').hover(function () {
        $('.mot-caption-fade', this).fadeIn('slow');
      }, function () {
        $('.mot-caption-fade', this).fadeOut('slow');
      });
    }
  };
}(jQuery));

function popupinfo(nid, time_inteval) {
  'use strict';
  jQuery.get(Drupal.settings.basePath + 'node/' + nid, function (data) {
    var node = jQuery(data).find('#node-' + nid);
    node.find('.back-mot').remove();
    jQuery('#team-member-info').empty().append(node);
    jQuery('#team-member-info').slideDown(1500).delay(time_inteval).slideUp(2000);
  });
  return false;
}
